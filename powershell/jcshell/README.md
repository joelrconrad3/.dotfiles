# Oh My Posh (omp) Install and Setup

1. Installation
2. Config

# Installation 

1. Install omp

    winget install JanDeDobbeleer.OhMyPosh

2. Install PSGallery that gives directory listing colors and icons

    Install-Module -Name Terminal-Icons -Repository PSGallery

Helpful Resources:

- [Hanselman.com](https://www.hanselman.com/blog/my-ultimate-powershell-prompt-with-oh-my-posh-and-the-windows-terminal)
- [Documentation](https://ohmyposh.dev/docs/windows)

# Configuration

To link your custom configuration with the omp .exe, use this command:

    oh-my-posh --init --shell pwsh --config C:/Users/joel.conrad/Documents/PowerShell/jcshell/config.json | Invoke-Expression 


# Calling omp on startup

1. Add code to $Profile

To find your startup file, type this in:
    
    echo $PROFILE

See [here] for documentation on Powershell Startup Config.
