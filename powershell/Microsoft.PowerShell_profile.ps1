#https://gist.github.com/shanselman/25f5550ad186189e0e68916c6d7f44c3 Dependencies

using namespace System.Management.Automation
using namespace System.Management.Automation.Language

if ($host.Name -eq 'ConsoleHost'){
    Import-Module PSReadLine
}

Import-Module -Name Terminal-Icons
#Import-Module oh-my-posh


oh-my-posh --init --shell pwsh --config $PSScriptRoot\jcshell\ohmyposh.json | Invoke-Expression

Register-ArgumentCompleter -Native -CommandName winget -ScriptBlock {
    param($wordToComplete, $commandAst, $cursorPosition)
        [Console]::InputEncoding = [Console]::OutputEncoding = $OutputEncoding = [System.Text.Utf8Encoding]::new()
        $Local:word = $wordToComplete.Replace('"', '""')
        $Local:ast = $commandAst.ToString().Replace('"', '""')
        winget complete --word="$Local:word" --commandline "$Local:ast" --position $cursorPosition | ForEach-Object {
            [System.Management.Automation.CompletionResult]::new($_, $_, 'ParameterValue', $_)
        }
}

# PowerShell parameter completion shim for the dotnet CLI
Register-ArgumentCompleter -Native -CommandName dotnet -ScriptBlock {
     param($commandName, $wordToComplete, $cursorPosition)
         dotnet complete --position $cursorPosition "$wordToComplete" | ForEach-Object {
            [System.Management.Automation.CompletionResult]::new($_, $_, 'ParameterValue', $_)
         }
 }

$basePath = "C:/Users/joel.conrad/Documents/PowerShell"

# Get Image
$imgDir = Join-Path -Path $basePath -ChildPath "imgs"
$imgs = Get-ChildItem -recurse ($imgDir) -File
$rando = Get-Random -Maximum $imgs.Length
$outputImg = $imgs[$rando]

$settings = Join-Path -Path $basePath -ChildPath "settings.json"

$getsettings = get-content $settings
$convertSettings = $getSettings | ConvertFrom-Json

$convertSettings.profiles.list[0].backgroundImage = $outputImg.FullName

ConvertTo-Json -InputObject $convertSettings -Depth 5 | out-file $settings
