# See if Oh My Posh is installed 
dotnet tool install --global PowerShell

New-Item -ItemType SymbolicLink -Path C:\Users\$env.UserName\AppData\Local\Packages\Microsoft.WindowsTerminal_8wekyb3d8bbwe\LocalState\settings.json -Target C:/Users/joel.conrad/Documents/Powershell/settings.json

winget install JanDeDobbeleer.OhMyPosh
    
Install-Module -Name Terminal-Icons -Repository PSGallery
