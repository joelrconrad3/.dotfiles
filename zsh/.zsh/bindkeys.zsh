# Empty command is converted to "ls"
function empty-buffer-to-ls() {
    if [[ $#BUFFER == 0 ]]; then
        BUFFER="ls" 
    fi
}

bindkey -v
export KEYTIMEOUT=1

# Change cursor with support for inside/outside tmux
function _set_cursor() {
    if [[ $TMUX = '' ]]; then
      echo -ne $1
    else
      echo -ne "\ePtmux;\e\e$1\e\\"
    fi
}

function _set_block_cursor() { _set_cursor '\e[2 q' }
function _set_beam_cursor() { _set_cursor '\e[6 q' }

function zle-keymap-select {
  if [[ ${KEYMAP} == vicmd ]] || [[ $1 = 'block' ]]; then
      _set_block_cursor
  else
      _set_beam_cursor
  fi
}


# BIND KEYS with zsh-vi-mode
function zvm_after_init(){
    zvm_bindkey viins '^I' autosuggest-accept
    # Ctrl+Space to see git status
    zvm_bindkey viins '^ ' git status
    # ctrl+backspace: delete word before
    zvm_bindkey viins '^H' backward-kill-word


    zle -N zle-line-finish zvm_config
    # call empty function
    zle -N zle-line-finish empty-buffer-to-ls
    # Disable the cursor style feature
    zle -N zle-keymap-select
    # ensure beam cursor when starting new terminal
    precmd_functions+=(_set_beam_cursor) #
    # ensure insert mode and beam cursor when exiting vim
    zle-line-init() { zle -K viins; _set_beam_cursor }
}

#########################
## Under Consideration ##
#########################

 #shift-tab: go backward in menu (invert of tab)
#bindkey '^[[Z' reverse-menu-complete

 #ctrl+delete: delete word after
#bindkey "\e[3;5~" kill-word

 #alt+m: copy last word
#bindkey "^[m" copy-prev-shell-word

## Execute the current suggestion (using zsh-autosuggestions)
## Alt+Enter = '^[^M' on recent VTE and '^[^J' for older (Lxterminal)
#bindkey '^[^M' autosuggest-execute
#bindkey '^[^J' autosuggest-execute

## Disable the capslock key and map it to escape
#setxkbmap -option caps:backspace

## set special widget, see man zshzle
##zle -N zle-line-finish empty-buffer-to-ls

## Disable flow control (ctrl+s, ctrl+q) to enable saving with ctrl+s in Vim
#stty -ixon -ixoff
