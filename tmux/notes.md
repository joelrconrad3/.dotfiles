Reload from a tmux file
    :source-file ~/.tmux.conf
    tmux source-file /file/path
    tmux source /file/path

Attach to an existing session
    tmux attach-session -t {number or name}
    :att -t {number or name}

Create new session
    tmux new-session -t {name or number}
    tmux new-session
    tmux

List previous sessions
    tmux list-sessions

Attach to the last session
    tmux a
    tmux attach-session

Exit from the session
    prefix d

Delete a session
    tmux kill-session

Delete a window
    prefix x
    tmux kill-window

# Plugins
## Resurrect
### Installation
Clone Tmux Plugin Manager
    git clone https://github.com/tmux-plugins/tmp ~/.tmux/plugins/tpm

### Use
To save a session
    prefix + Ctrl + s

To restore a session
    prefix + Ctrl + r
