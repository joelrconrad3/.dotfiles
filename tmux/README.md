# Installing plugin manager
    git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm

# Documentation
See [here](https://github.com/tmux-plugins/tpm) for more information.

# Installing a plugin
    prefix + I
    tmux source ~/.tmux.conf
