#!/bin/bash
fontdump=~/.local/share/fonts
mkdir $fontdump
curl -L https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Regular.ttf -o $fontdump/MesloNFRegular.ttf
curl -L https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold.ttf -o $fontdump/MesloNFBold.ttf
curl -L https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Italic.ttf -o $fontdump/MesloNFItalic.ttf
curl -L https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold%20Italic.ttf -o $fontdump/MesloNFBoldItalic.ttf
fc-cache -f -v
