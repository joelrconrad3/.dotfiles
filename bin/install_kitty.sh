#!/bin/bash
curl -L https://sw.kovidgoyal.net/kitty/installer.sh | sh /dev/stdin

sudo ln -s $HOME/.local/kitty.app/bin/kitty /usr/bin/kitty

sudo update-alternatives --install /usr/bin/x-terminal-emulator x-terminal-emulator $HOME/.local/kitty.app/bin/kitty 50

# Show all available terminals
#sudo update-alternatives --config x-terminal-emulator
