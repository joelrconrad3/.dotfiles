#!/bin/bash
while getopts lipa flag
do
    case "${flag}" in
    esac
done

sudo apt install \
    # Personal
    cmatrix
    qutebrowser
    fzf
    # Python
    python3-dev
    libpq-dev
    python3-pip
    # Neovim specific
    ninja-build 
    gettext 
    libtool 
    libtool-bin 
    autoconf 
    automake 
    cmake 
    g++ 
    pkg-config 
    unzip 
    curl 
    doxygen
