#!/bin/bash
# Link files
while getopts lipa flag
do
    case "${flag}" in
        l)
            ln -s ~/.dotfiles/zsh/.zshrc ~/.zshrc
            ln -s ~/.dotfiles/zsh/.zsh ~/.zsh
            ln -s ~/.dotfiles/zsh/.zshenv ~/.zshenv
            ln -s ~/.dotfiles/zsh/.p10k.zsh ~/.p10k.zsh
            ;;
        i)
            sudo apt install zsh
            chsh -s $(which zsh)
            sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
            ;;
        p)
            git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
            git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
            git clone https://github.com/jeffreytse/zsh-vi-mode ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/plugins/zsh-vi-mode
            git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
            ;;
        a)
            ln -s ~/.dotfiles/zsh/.zshrc ~/.zshr
            ln -s ~/.dotfiles/zsh/.zsh ~/.zsh
            ln -s ~/.dotfiles/zsh/.zshenv ~/.zshenv
            ln -s ~/.dotfiles/zsh/.p10k.zsh ~/.p10k.zsh
            sudo apt install zsh
            chsh -s $(which zsh)
            sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
            git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
            git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
            git clone https://github.com/jeffreytse/zsh-vi-mode ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/plugins/zsh-vi-mode
            git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
    esac
done
