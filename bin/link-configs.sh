# !/bin/bash
while getopts ailp flag
do
    case "${flag}" in
        nvim)
            ln -s ~/.dotfiles/nvim ~/.config/nvim;;
        zsh)
            ln -s ~/.dotfiles/zsh/.zshrc ~/.zshrc
            ln -s ~/.dotfiles/zsh/.zsh ~/.zsh
            ln -s ~/.dotfiles/zsh/.zshenv ~/.zshenv
            ln -s ~/.dotfiles/zsh/.p10k.zsh ~/.p10k.zsh
            ;;
        i3)
            ln -s ~/.dotfiles/.i3 ~/.i3
            ln -s ~/.dotfiles/polybar ~/.config/polybar
            ;;
        tmux)
            ln -s ~/.dotfiles/tmux/.tmux.conf
            ;;
        git)
            ln -s ~/.dotfiles/git/.gitconfig ~/.gitconfig
            ;;
        qute)
            ln -s ~/.dotfiles/qutebrowser ~/.config/qutebrowser
            ;;
esac
done

