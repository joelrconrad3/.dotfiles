# PowerShell Config

In this configuration, you will find the following:

1. [PowerShell Startup Configuration](#Powershell Startup Configuration)

2. [Windows Terminal Settings](#Windows Terminal Settings)

3. [Oh-my-posh settings](./jcshell/README.md)

# Powershell Startup Configuration

This is the .ps1 file that is run upon starting the powershell.  This was
largely inspired and taken form
[hanselman.com](https://www.hanselman.com/blog/my-ultimate-powershell-prompt-with-oh-my-posh-and-the-windows-terminal)
and his [GitHub
Gist](https://gist.github.com/shanselman/25f5550ad186189e0e68916c6d7f44c3).

Additional settings can be configured.  See
[documentation](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_profiles?view=powershell-7.1)
for more details.

# Windows Terminal Settings 

settings.json file used as the config for Windows Terminal.  See:

    C:\Users\$USERPROFILE\AppData\Local\Packages\Microsoft.WindowsTerminal*\LocalState\settings.json

    New-Item -ItemType SymbolicLink -Path C:\Users\joel.conrad\AppData\Local\Packages\Microsoft.WindowsTerminal_8wekyb3d8bbwe\LocalState\settings.json -Target C:/Users/joel.conrad/Documents/Powershell/settings.json

for the preconfigured settings.  Copy and paste from this file.

# Installing Java Dev Environment
## Install java dev packages
    sudo apt install default-jre
    sudo apt install default-jdk
## Install Maven
    sudo apt install maven

## Get location of jdk executable
    which javac

Note: the location is a symbolic link... get source. Potentially using the following:
    readlink -f `which javac` | sed "s:/bin/javac::"

## Set JAVA_HOME Variable
In your .bashrc, put the following:
    export JAVA_HOME=/usr/lib/jvm/{other location information}/bin/java
