-----------------
--- Telescope ---
-----------------
require('telescope').setup{
    defaults = {
        vimgrep_arguments = {
         'rg',
         '--color=never',
         '--no-heading',
         '--with-filename',
         '--line-number',
         '--column',
         '--smart-case',
         '--ignore-file',
         '.gitignore'
        },
        file_ignore_patterns = {
            "**/*/*.egg-info/*",
            "node_modules/*",
            "obj/*",
            "*/bin/*",
            "*.vs",
            "__pycache__/.*",
            "**/__pycache__/**/*",
            ".work/.*",
            ".cache/.*",
            ".idea/.*",
            "dist/.*",
            ".git/*",
            "*.class",
            "*.jar",
            "*.lst",
            "venv/*/",
            "target/*",
            ".next/*",
        }
    },
    pickers = {
        find_files = {
            hidden = true
        }
    }
}
