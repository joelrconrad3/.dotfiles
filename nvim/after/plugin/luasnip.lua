local ls = require "luasnip"

require 'snippets.python'
require 'snippets.go'
require 'snippets.markdown'
require 'snippets.sh'
require 'snippets.all'
require 'snippets.typescript'
require 'snippets.csharp'
require 'snippets.java'
require 'snippets.rust'
require 'snippets.racket'
require 'snippets.javascript'
require 'snippets.clang'

vim.keymap.set({"i", "s"}, "<c-j>", function()
    if ls.expand_or_jumpable() then
        ls.expand_or_jump()
    end
end, {silent = true})

ls.config.set_config {
    history = true,
    -- treesitter-hl has 100, use something higher (default is 200).
    ext_base_prio = 200,
    -- minimal increase in priority.
    ext_prio_increase = 1,
    enable_autosnippets = false,
    store_selection_keys = "<c-s>",
}

ls.add_snippets(nil, {
    all = allSnip,
    sh = shSnip,
    python = pythonSnip,
    go = goSnip,
    markdown = markdownSnip,
    typescript = tsSnip,
    javascript = jsSnip,
    cs = csharpSnip,
    c = clangSnip,
    java = javaSnip,
    rust = rustSnip,
    racket = racketSnip,
})
