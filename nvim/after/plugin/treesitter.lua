require'nvim-treesitter.configs'.setup{
    highlight = {
        enable = true,
        additional_vim_syntax_highlighting=false
    },
}
