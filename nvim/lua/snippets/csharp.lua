local ls = require "luasnip"
local text = ls.text_node
local insert = ls.insert_node
local snip = ls.snippet

csharpSnip = {
    snip("pr", {
        text "Console.WriteLine(",
        insert(1),
        text ");"
    }),
    snip("try", {
        text {"try{" , "\t"},
        insert(1, "tryStatement"),
        text {"", "} catch(Exception){" , "\t" },
        insert(0, "catchStatement"),
        text {"", "}"},
    })
}
