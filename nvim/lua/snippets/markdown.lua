local ls = require "luasnip"
local func = ls.function_node
local text = ls.text_node
local insert = ls.insert_node
local snip = ls.snippet

function table_to_string(tbl)
    local result = "{"
    for k, v in pairs(tbl) do
        -- Check the key type (ignore any numerical keys - assume its an array)
        if type(k) == "string" then
            result = result.."[\""..k.."\"]".."="
        end

        -- Check the value type
        if type(v) == "table" then
            result = result..table_to_string(v)
        elseif type(v) == "boolean" then
            result = result..tostring(v)
        else
            result = result.."\""..v.."\""
        end
        result = result..","
    end
    -- Remove leading commas from the result
    if result ~= "" then
        result = result:sub(1, result:len()-1)
    end
    return result.."}"
end

markdownSnip = {
    -- Select link, press C-s, enter link to receive snippet
    snip({
        trig = "zkpersonal",
        namr = "zk link to personal",
        dscr = "Create markdown link [txt](url)",
    },{
        text "../topics/personal/",
        insert(0)
    }),
    snip({
        trig = "zkprofesional",
        namr = "zk link to personal",
        dscr = "Create markdown link [txt](url)",
    },{
        text "../topics/professional/",
        insert(0)
    }),
    snip({
        trig = "zkdaily",
        namr = "zk link to daily",
        dscr = "Create markdown link [txt](url)",
    },{
        text "../../../daily/" ,
        text {os.date("%m%d%y")},
        text ".md",
        insert(0)
    }),
    snip({
        trig = "link",
        namr = "markdown_link",
        dscr = "Create markdown link [txt](url)",
    }, {
        text "[",
        insert(2, "text"),
        text "](",
        insert(1, "url"),
        text ")",
        insert(0),
    }),
    snip({
        trig = "codewrap",
        namr = "markdown_code_wrap",
        dscr = "Create markdown code block from existing text",
    }, {
        text "``` ",
        insert(1, "Language"),
        text { "", "" },
        func(function(_, snip)
            local tmp = {}
            tmp = snip.env.TM_SELECTED_TEXT
            tmp[0] = nil
            return tmp or {}
        end, {}),
        text { "", "```", "" },
        insert(0),
    }),
    snip({
        trig = "codeempty",
        namr = "markdown_code_empty",
        dscr = "Create empty markdown code block",
    }, {
        text "``` ",
        insert(1, "Language"),
        text { "", "" },
        insert(2, "Content"),
        text { "", "```", "" },
        insert(0),
    }),
    snip({
        trig = "meta",
        namr = "Metadata",
        dscr = "Yaml metadata format for markdown",
    }, {
        text { "---", "title: " },
        insert(1, "note_title"),
        text { "", "author: " },
        insert(2, "author"),
        text { "", "date: " },
        func(date, {}),
        text { "", "cathegories: [" },
        insert(3, ""),
        text { "]", "lastmod: " },
        func(date, {}),
        text { "", "tags: [" },
        insert(4),
        text { "]", "comments: true", "---", "" },
        insert(0),
    }),
    snip({
        trig = "sweek",
        namr = "Set Week",
        dscr = "Set up the week",
    }, {
        text { "# Monday", "", "# Tuesday", "", "# Wednesday", "", "# Thursday" , "", "# Friday" }
    })
}
