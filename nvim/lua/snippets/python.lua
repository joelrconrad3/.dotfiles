local ls = require "luasnip"

local text = ls.text_node
local insert = ls.insert_node
local snip = ls.snippet

pythonSnip = {
    snip("shebang", {
        text { "#!/usr/bin/env python", "" },
        insert(0),
    }),
    snip("pr", {
        text "print(",
        insert(1, "text"),
        text {")"},
	insert(0)
    }),
    snip("fx", {
       text "def ",
       insert(1, "fxName"),
       text "(",
       insert(2, "fxParams"),
       text { "):", "" },
       text "\t",
       insert(0),
    }),
-------------
--UnitTests--
-------------
    snip("utestscratch", {
        text {"import unittest", "", ""},
        text "class ",
        insert(1, "TestCaseName"),
        text {"(unittest.TestCase):", ""},
        text "\tdef ",
        insert(2, "Test Name"),
        text {"(self):", "\t\t"},
        insert(3, "TestBody"),
        text {"", "\t\tpass"},
        insert(0)
    }),
    snip("utestclass", {
        text "class ",
        insert(1, "TestCaseName"),
        text {"(unnittest.TestCase):", ""},
        text "\tdef ",
        insert(2, "Test Name"),
        text {"(self):", "\t\t"},
        insert(3, "TestBody"),
        text {"", "\t\tpass"},
        insert(0)
    }),
    snip("utest", {
        text "\tdef ",
        insert(1, "Test Name"),
        text {"(self):", "\t\t"},
        insert(2, "TestBody"),
        text {"", "\t\tpass"},
        insert(0)
    }),
---------
--ArcPy--
---------
    snip("scursor", {
        text "with da.SearchCursor(",
        insert(1, "intable"),
        text ", ",
        insert(2, "field_names"),
        text ", where_clause=",
        insert(3, "None"),
        text ", spatial_reference=", 
        insert(4, "None"),
        text ", explode_to_points=",
        insert(5, "None"),
        text ", sql_clause=",
        insert(6, "(None"),
        insert(7, ", None)"),
        text ", datum_transformation=",
        insert(8, "None"),
        text {") as cursor:", "\tfor row in cursor:", "\t\t"},
        insert(0)
    }),
    snip("icursor", {
        text "cursor = da.InsertCursor(",
        insert(1, "intable"),
        text ", ",
        insert(2, "field_names"),
        text ", datum_transformation=",
        insert(3, "None)"),
        insert(0)
    })
}
