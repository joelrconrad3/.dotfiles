local ls = require "luasnip"

local text = ls.text_node
local insert = ls.insert_node
local snip = ls.snippet


racketSnip = {
    snip("shebang", {
        text { "#lang racket", "" },
        insert(0),
    }),
}
