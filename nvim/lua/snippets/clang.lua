local ls = require "luasnip"

local text = ls.text_node
local insert = ls.insert_node
local snip = ls.snippet

clangSnip = {
    snip("pr", {
        text "printf(\"",
        insert(1, "text"),
        text {"\\n\");"},
	    insert(0)
    }),
}
