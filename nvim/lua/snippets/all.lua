local ls = require "luasnip"
local text = ls.text_node
local insert = ls.insert_node
local snip = ls.snippet
local func = ls.function_node

local timestamp = function()
    return { os.date "%m%d%y.%H.%M" }
end
local day = function()
    return { os.date "%m%d%y" }
end
-- Make sure to not pass an invalid command, as io.popen() may write over nvim-text.
local function bash(_, _, command)
    local file = io.popen(command, "r")
    local res = {}
    for line in file:lines() do
        table.insert(res, line)
    end
    return res
end

allSnip = {
    snip({
        trig = "stamp",
        namr = "stamp",
        dscr = "TimeStamp in the form of YYMMDD.H.M",
    }, {
        func(timestamp, {}),
    }),
    snip({
        trig = "day",
        namr = "Day",
        dscr = "Day in the form of YYMMDD",
    }, {
        func(day, {}),
    }),
    snip({
        trig = "pwd",
        namr = "PWD",
        dscr = "Path to current working directory",
    }, {
        func(bash, {}, "pwd"),
    }),
    snip({
        trig = "signature",
        namr = "Signature",
        dscr = "Name and Surname",
    }, {
        text "Joel Conrad",
        insert(0),
    }),
}
