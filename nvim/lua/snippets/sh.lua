local ls = require "luasnip"

local text = ls.text_node
local insert = ls.insert_node
local snip = ls.snippet

shSnip = {
    snip("shebang", {
        text { "#!/bin/sh", "" },
        insert(0),
    }),
    snip("flags", {
        text "while getopts ",
        insert(1, "*flags*"),
        text {" flag", ""},
        text{ "do", ""},
        text "\tcase \"${flag}\" in",
        text{"", "\tesac", "done"},
        insert(0)
    })
}
