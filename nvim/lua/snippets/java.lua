
local ls = require "luasnip"

local text = ls.text_node
local insert = ls.insert_node
local snip = ls.snippet

javaSnip = {
    snip("pr", {
        text "System.out.println(",
        insert(1),
        text ");"
    }),
}
