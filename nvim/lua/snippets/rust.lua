local ls = require "luasnip"

local text = ls.text_node
local insert = ls.insert_node
local snip = ls.snippet

rustSnip = {
    snip("pr", {
        text "println!(\"{",
        insert(1, ""),
        text "}\", ",
        insert(2, ""),
        text {");"},
	    insert(0)
    }),
}
