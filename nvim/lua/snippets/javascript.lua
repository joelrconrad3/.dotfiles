local ls = require "luasnip"

local text = ls.text_node
local insert = ls.insert_node
local snip = ls.snippet

jsSnip = {
    snip("pr", {
        text "console.log(",
        insert(1, "text"),
        text {")"},
	      insert(0)
    }),
}
