-------------
-- Options --
-------------
vim.opt.cmdheight = 2
--vim.opt.colorcolumn = '80'
vim.opt.expandtab = true
vim.opt.exrc = true
vim.opt.errorbells = false
--vim.opt.guicursor = 'a:block-Caret'
--vim.opt.guicursor = 'a:hor30-iCursor-blinkwait300-blinkon200-blinkoff150'
vim.opt.hidden = true
vim.opt.incsearch = true
vim.opt.inccommand = 'nosplit'
vim.opt.incsearch = true
vim.opt.number = true
vim.opt.wrap = false
vim.opt.hlsearch = false
vim.opt.relativenumber = true
vim.cmd("au BufEnter * set relativenumber")
vim.opt.scrolloff = 4
vim.opt.shiftwidth = 4
vim.opt.shortmess = vim.opt.shortmess + 'c'
vim.opt.showmode = false
vim.opt.signcolumn = 'yes'
vim.opt.smartindent = true
vim.opt.splitbelow = true
vim.opt.splitright = true
vim.opt.swapfile = false
vim.opt.softtabstop = 2
vim.opt.backup = false
vim.opt.tabstop = 2
vim.opt.termguicolors= true
vim.opt.wrap = false 
vim.opt.ruler = true
vim.opt.background = 'dark'
--vim.opt.undodir='~/.vim/undodir'
vim.opt.undofile = true
vim.opt.timeoutlen = 300
vim.opt.ttimeoutlen = 5

-- Completion
vim.opt.completeopt = 'menu,preview,menuone,noselect'

-- V important: sets the background to transparent
vim.cmd("hi Normal guibg=NONE ctermbg=NONE")
vim.cmd("au ColorScheme * hi Normal ctermbg=none guibg=none")

vim.cmd('let g:netrw_bufsettings="noma nomod nonu nobl nowrap ro rnu"')
vim.cmd('let g:netrw_browse_split=0')
vim.cmd('let g:netrw_banner=0')
vim.cmd('let g:netrw_winsize=25')

-- Nice menu when typing :find *.py
vim.opt.wildmenu = true
vim.opt.wildmode = 'longest,list,full'
-- Ignore files
vim.opt.wildignore = {'*.pyc', '*_bild/*', '**/coverage/*', '**/node_modules/*', '**/android/*', '**/ios/*', '**/.git/'}

vim.o.inccommand = 'nosplit'
--vim.o.completevim ='menuone,noselect'

-----------------
--Under Review---
-----------------
--[[vim.opt.tabstop = 4
vim.opt.termguicolors = true
vim.opt.undofile = true
vim.opt.updatetime = 100
vim.opt.ignorecase = true
vim.opt.backspace = 'indent,eol,start'
vim.opt.hls = true
vim.opt.is = true
]]

---------------------
--Window Navigation--
---------------------
vim.cmd(
    [[ 
        let i = 1
        while i <= 12
            execute 'nnoremap <Leader>' . i . ' :' . i . 'wincmd w<CR>'
            let i = i + 1
        endwhile 
    ]],
    false
)

