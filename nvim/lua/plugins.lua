local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

local leet_arg = "leetcode.nvim"

-----------
--Plugins--
-----------
-- Example using a list of specs with the default options
vim.g.mapleader = " " -- Make sure to set `mapleader` before lazy so your mappings are correct

require("lazy").setup({
    -- Utility plugins
    { "ellisonleao/gruvbox.nvim" },
    'nvim-lua/plenary.nvim',
    'lewis6991/gitsigns.nvim',
    {
        'VonHeikemen/lsp-zero.nvim',
        branch = 'v2.x',
        dependencies = {
          -- LSP Support
          {'neovim/nvim-lspconfig'},             -- Required
          {'williamboman/mason.nvim'},           -- Optional
          {'williamboman/mason-lspconfig.nvim'}, -- Optional

          -- Autocompletion
          {'hrsh7th/nvim-cmp'},     -- Required
          {'hrsh7th/cmp-nvim-lsp'}, -- Required
          {'L3MON4D3/LuaSnip'},     -- Required
        }
    },

    -- Commenter
    {
        'numToStr/Comment.nvim',
        opts = {
            -- add any options here
        },
        lazy = false,
    },

    'windwp/nvim-autopairs',
    'windwp/nvim-ts-autotag',

    -- Lua
    {
      "folke/trouble.nvim",
      dependencies = "nvim-tree/nvim-web-devicons",
      opts = {
      },
    },

    ({
        "kylechui/nvim-surround",
        -- version = "*", -- Use for stability; omit to use `main` branch for the latest features
        config = function()
            require("nvim-surround").setup({
                -- Configuration here, or leave empty to use defaults
            })
            end
    }),

    -- Java lsp
    'mfussenegger/nvim-jdtls',

    -- Treesitter
    {
        'nvim-treesitter/nvim-treesitter',
        run = ":TSUpdate"
    },

    -- -- Git related plugins
    'lewis6991/gitsigns.nvim',
    'tpope/vim-fugitive',
    --
    -- -- Aesthetic related plugins
    'nvim-tree/nvim-web-devicons',
    {
        'nvim-lualine/lualine.nvim',
        dependencies = { 'nvim-tree/nvim-web-devicons', opt = true }
    },
    {
	      "L3MON4D3/LuaSnip",
	      version = "2.*", -- Replace <CurrentMajor> by the latest released major (first number of latest release)
    },
    'saadparwaiz1/cmp_luasnip',
    -- File navigation
    {
        "ThePrimeagen/harpoon",
        branch = "harpoon2",
        requires = { {"nvim-lua/plenary.nvim"} }
    },
    {
      'nvim-telescope/telescope.nvim',
      dependencies = { {'nvim-lua/plenary.nvim'} }
    },
    'troydm/zoomwintab.vim',
    {
        'glacambre/firenvim',

        lazy = not vim.g.started_by_firenvim,
        build = function()
            vim.fn["firenvim#install"](0)
            vim.g.firenvim_config.localSettings['.*'] = { takeover = 'never' }
        end
    },

    {
        "aca/emmet-ls"
    },
    -- Leetcode
    {
        "kawre/leetcode.nvim",
        build = ":TSUpdate html",
        dependencies = {
            "nvim-telescope/telescope.nvim",
            "nvim-lua/plenary.nvim", -- required by telescope
            "MunifTanjim/nui.nvim",

            -- optional
            "nvim-treesitter/nvim-treesitter",
            "rcarriga/nvim-notify",
            "nvim-tree/nvim-web-devicons",
        },
        opts = {
            arg = "leetcode.nvim",
            lang = "python3"
        }
    },
    {
        "chentoast/marks.nvim",
        event = "VeryLazy",
        opts = {},
    }
})
