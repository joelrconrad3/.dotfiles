vim.g.mapleader = " "
-- general
vim.keymap.set("n", "<leader>c", ":set clipboard=unnamed<CR>")
vim.keymap.set("n", "<leader>pv", vim.cmd.Ex)
vim.keymap.set("n", "<leader>dui", ":lua require('dapui').toggle()<CR>", {noremap=true, silent=true})



vim.cmd.colorscheme("gruvbox")
-------------
--Remaps-----
-------------

--Window Movements --
vim.keymap.set("n", "<C-j>", "<C-W>j")
vim.keymap.set("n", "<C-k>", "<C-W>k")
vim.keymap.set("n", "<C-l>", "<C-W>l")
vim.keymap.set("n", "<C-h>", "<C-W>h")

-- Directory Changes --
vim.api.nvim_set_keymap("n", "<leader>cd", ":cd ", {noremap=true, silent=true})
vim.api.nvim_set_keymap("n", "<leader>.", ":e.<CR>", {noremap=true, silent=true})

--Telescope--
vim.keymap.set("n", "<leader>f", ":Telescope find_files<cr>")

--Exit searches
vim.api.nvim_set_keymap("n", "<Esc>", ":noh<return><Esc>", {noremap=true, silent=true})
--Exit terminal
vim.api.nvim_set_keymap("t", "<Esc>", "<C-\\><C-n>", {noremap=true, silent=true})


-----------------
--vim-fugitive---
-----------------
vim.api.nvim_set_keymap('n', '<leader>-', ':Gdiffsplit <CR>', {noremap=true, silent=true})
vim.api.nvim_set_keymap('n', '<leader>g', ':G <CR>', {noremap=true, silent=true})
vim.api.nvim_set_keymap('n', '<leader>ga', ':G add', {noremap=true, silent=true})
vim.api.nvim_set_keymap('n', '<leader>gc', ':G commit<CR>', {noremap=true, silent=true})
vim.api.nvim_set_keymap('n', '<leader>gpl', ':G pull<CR>', {noremap=true, silent=true})
vim.api.nvim_set_keymap('n', '<leader>gps', ':G push<CR>', {noremap=true, silent=true})

----------------
---ZoomWinTab---
----------------
vim.keymap.set("n", "<leader>zm", ":ZoomWinTabToggle<CR>")

---------------------
--Window Navigation--
---------------------
vim.keymap.set("n", "<leader>=", "<C-W>=")

--------------
-- NVIM-DAP --
--------------
vim.keymap.set("n", "<leader>c", ":lua require'dap'.continue()<CR>")
vim.keymap.set("n", "<leader>b", function() require('dap').toggle_breakpoint() end)
vim.keymap.set("n", "<leader>o", ":lua require'dap'.step_over()<CR>")
vim.keymap.set("n", "<leader>i", ":lua require'dap'.step_into()<CR>")
vim.keymap.set("n", "<leader>dap", ":lua require'dap'.repl.open()<CR>")

-- FOR JDTLS Only --
-- Explanation: Must run after Lsp has initiated.  Difficult to configure in config. -- 
vim.api.nvim_set_keymap("n", "<leader>ds", ":lua require('jdtls').setup_dap({ hotcodereplace = 'auto' })<CR>:lua require'jdtls.dap'.setup_dap_main_class_configs()<CR>", {noremap=true, silent=true})

------------
-- DAP-UI --
------------
-- vim.api.nvim_set_keymap("n", "<leader>dui", ":lua require('dapui').toggle()<CR>", {noremap=true, silent=true})
