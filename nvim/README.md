# TLDR 

1. [Installation](#Installing neovim)

2. [Nerd Font](#How to install Nerd font to show icons)

3. [LSP](#Current lsp installed on config and how to install them)

4. [Tree sitter](#Nvim Treesitter)

5. [P & A](#P&A)

# Installing neovim
-From Choco | Windows	
	choco install neovim 
- From Brew | Mac
	brew install neovim

# How to install Nerd font to show icons 
- Go to [Nerd fonts](https://github.com/ryanoasis/nerd-fonts#font-installation)
## For MacOs
  brew tap homebrew/cask-fonts
  brew install --cask font-hack-nerd-font
## For Windows
    ./install.ps1 Hack

# Current lsp installed on config and how to install them.
1. [jedi language server](https://github.com/neovim/nvim-lspconfig/blob/master/CONFIG.md#pyright)
    pip install -U jedi_lanugage_server
2. [Tsserver](https://github.com/neovim/nvim-lspconfig/blob/master/CONFIG.md#tsserver)
    npm install -g typescript typescript-language-server
*Note:* Server will only work with a tsconfig.json or jsconfig.json at the root of the project/directory.
    {
      "compilerOptions": {
        "module": "commonjs",
        "target": "es6",
        "checkJs": false
      },
      "exclude": [
        "node_modules"
      ]
    }
3. ccls
### [macos](https://github.com/MaskRay/ccls/wiki/Build#macos)
    brew update
    brew install ccls
### [windows](https://github.com/MaskRay/ccls/wiki/Build#windows)
    choco install llvm
    choco instlll ccls
4. [Deno](https://github.com/denoland/deno)
### Mac os
    brew install deno
### Windows
    choco install deno
5. [Csharp_ls](https://github.com/razzmatazz/csharp-language-server)
    dotnet tool install --global csharp-ls
- Install successful but get the error "cmd {csharp-ls] is not executable, when I go to edit a file.
6. [cssls](https://github.com/hrsh7th/vscode-langservers-extracted)
    npm install -g vscode-langservers-extracted
7. [html](https://github.com/hrsh7th/vscode-langservers-extracted)
- comes with cssls, just add it to the list of servers
8. Java Lang server
References:
- [Eclipse.jdt.ls](https://github.com/eclipse/eclipse.jdt.ls)
- [Associated Plugin](https://github.com/mfussenegger/nvim-jdtls)
1. Install Java
2. (I chose to) Build from source
    git clone https://github.com/eclipse/eclipse.jdt.ls.git
    $JAVA_HOME ./mvnw clean verify
Otherwise, follow the instructions in the documentation to complete the process

## Honorable mention
- [sqls](https://github.com/lighttiger2505/sqls)
    npm install -g sql-language-server
Only supports MySQL, PostgresSQL, and SQLite3

# Nvim Treesitter
Requirements:
1. Neovim nightly 
2. Git
3. C compiler

## For Windows 

    choco install llvm 
    choco install neovim -pre

## Installing modules

    :TSInstall python

# Configuring MikTex
## Installation

    choco install miktex
    choco install sumatrapdf

## Dependencies
    choco install activeperl

# Helpful commands
	:PackerInstall:
    :PackerClean
    :options

# NVIM nightly from GitHub
## Installation
[Official Link](https://neovim.discourse.group/t/nightly-build-on-wsl-2/171/2)
1. Update
    sudo apt update -y
2. Prerequisites
    sudo apt install -y ninja-build gettext libtool libtool-bin autoconf automake cmake g++ pkg-config unzip 
3. File config and git clone.
    cd; mkdir clones; cd clones; git clone https://github.com/neovim/neovim.git; cd neovim
4. Checkout out the desired version
    git tag
    git checkout tags/desired_version_number -b tagName-branch
(or just use master for latest)
5. Remove existing install
    rm -rf build
6. Make the projects
    make CMAKE_BUILD_TYPE=Release
7. Install the executable.
    sudo make install
8. (If you made a new branch) Destroy branch
    git branch -D tagName-branch
## Uninstall
Delete directories where data is installed.
    sudo rm /usr/local/bin/nvim
    sudo rm -r /usr/local/share/nvim/

# [Distant nvim](https://github.com/chipsenkbeil/distant.nvim)
Distant allows development of remote files inside a local development environment via ssh and distant.

To use, do the following: 
1. Install [Distant](https://github.com/chipsenkbeil/distant)
2. Add Distant plugin

# General Structure
init.lua -> lua -> map.lua | options.lua | plugins.lua

## Lexical
- drop down from lexical

# Credit
- Window Navigation function, [abcd](https://stackoverflow.com/questions/6403716/how-to-make-a-shortcut-for-moving-between-vim-windows)

# P&A

## Nvim-TS-Autopairs wasn't working on install

Make sure that the appropiate Treesitter is installed for filetype
    
    :TSInstall html
